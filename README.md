# RazHome Scripts

Various system tasks and scripts for initial setup, runtime, and package installation

## Initial Setup
`install.sh` \[WIP\] will install razhome to the /home/pi/razhome directory. It should be `curl`ed directly from github and run on a fresh install of raspbian. This may change in future.

## Post-Setup Runtime
The `/runtime` directory contains a set of services and scripts that are run at various times during each razhome session. The script `services.sh` will install/update all the services
### Services
*In the `/runtime/services` directory, there are a collection of `systemd` services which are either run at boot or at shutdown.*

**`razhome-db.service`** - This service runs the database http server, [RazHome DB API](https://gitlab.com/razhome/razhome-db-api), which is used by various applications in the suite to access the MongoDB database. It also will log the status and IP address of the Pi at startup.

**`razhome-shutdown.service`** - This service logs the fact that the pi is shutting down to the database.

**`razhome-speech.service`** - This service runs the Speech-to-Text/Text-to-Speech package, [Razhome Speech Daemon](https://gitlab.com/razhome/razhome-db-api), which is used for command reading and parsing, as well as verbal command responses.

## Scripts and Cron Tasks
*In the `/scripts` directory, you will find some scripts and the cron file `razhome`. These are oneoff tasks that are run at various times during the uptime of the RazHome device. Tasks which are only one command are not given a separate script and will be documented under Cron Tasks and are referenced by line number*

#### Scripts
**`log-shutdown.sh`** - Called by `razhome-shutdown.service`

**`update-db.sh`** - Called every 5 minutes in cron. Updates the PiStatus db with status "Ping"

#### Single-Line Cron Tasks
**Line 1** - Calls `update-db.sh` every 5 minutes
