#!/bin/bash
ip=`/bin/hostname -I | /bin/grep -Eo --color=never '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'`
/usr/bin/curl --data "ip=$ip&status=Ping" localhost:3001/api/status
