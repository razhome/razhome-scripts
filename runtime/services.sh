#!/bin/bash
# Installs all services to desired location.
# If a service already exists, it will overwrite it
# Must be run as superuser

set -e

if [ $UID != 0 ]; then
    echo "This script must be run as root"
    exit 1
fi

# prompt for confirmation
echo "Warning: This will overwrite existing installs of these services and cron tasks."
echo -n "Are you sure you want to continue? [y/N]: "
read cont
if [ $0 != "-y" ] && [ $cont != "y" ] && [ $cont != "Y" ]; then
    echo "exiting..."
    exit 0
fi

# install the service stored in $currentService
installService() {
    if [ -f /etc/systemd/system/$currentService ]; then
        echo "Truncating existing service '$currentService'"
        systemctl disable $currentService
    fi
    cp -v services/$currentService /etc/systemd/system/$currentService
    systemctl enable $currentService
    echo "Installed $currentService"
}

# install services
echo "Installing and enabling services"
currentService=razhome-db.service
installService

currentService=razhome-shutdown.service
installService

currentService=razhome-speech.service
installService

currentService=razhome-startup.service
installService

currentService=razhome-updater.service
installService

# install cron.d files
echo "Adding cron files to /etc/cron.d"
cp -v scripts/razhome /etc/cron.d/razhome

echo "Ensuring all scripts can be executed"
chmod +x scripts/*.sh
