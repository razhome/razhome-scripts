#!/bin/bash
set -e
pack=$1
packdir=/home/pi/razhome/packages/$1
servicefile="$(echo $pack | sed 's/\./-/g').service"

echoerr() { echo >&2 "$@"; }

exiterr() {
    echo >&2 "Exited with error status $@"
}

if [ $UID != 0 ]; then
    echoerr "ERROR: this script must be run as root"
    exiterr 1
fi

if [ ! -d $packdir ]; then
    echoerr "ERROR: Package not found"
    exiterr 2
fi

# if service exists, disable and delete
if [ -f "/etc/systemd/service/$servicefile"]; then
    systemctl disable $servicefile
    rm -f "/etc/systemd/service/$servicefile";
fi

# delete package files
rm -rf $packdir

# delete package on database
curl -X DELETE http://localhost:3001/api/packs/ns/$pack