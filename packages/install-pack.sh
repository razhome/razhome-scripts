#!/bin/bash
set -e
pack=$1

echoerr() { echo >&2 "$@"; }

exiterr() {
    echo >&2 "Exited with error status $@"
}

# installs dependancies in pack.json for the given source in $dep_source (apt or pip)
getpackname() {
    python3 -c "import json, pathlib; deps=json.loads(pathlib.Path('pack.json').read_text())['deps']; print(' '.join(deps['$dep_source']) if deps['$dep_source'] else 'NONE')"
}

# install the service stored in $servicefile
installService() {
    if [ -f /etc/systemd/system/$servicefile ]; then
        echo "Truncating existing service $servicefile"
        systemctl disable $servicefile
    fi
    cp -v $packdir/$servicefile /etc/systemd/system/$servicefile
    systemctl enable $servicefile
    echo "Installed $servicefile"
}

# installs all deps stored in $service

if [ $UID != 0 ]; then
    echoerr "ERROR: this script must be run as root"
    exiterr 1
fi

if [ ! -f $pack ]; then
    echo "ERROR: file $pack not found"
    exiterr 2
fi

# unpack the pack file in the proper directory
packns=$(tar -axf $pack pack.json -O | grep "namespace" | sed -E 's/\W*namespace\W*([A-Za-z\.]*).*/\1/g') || exiterr 2
packdir="/home/pi/razhome/packages/$packns"
echo "Extracting to $packdir"
mkdir /home/pi/razhome/packages/$packns
tar -xvf $pack -C /home/pi/razhome/packages/$packns
cd $packdir

# install dependancies
# apt
aptdeps=$(python3 /home/pi/razhome/razhome-scripts/packages/get_deps.py $packdir/pack.json 'apt')
pipdeps=$(python3 /home/pi/razhome/razhome-scripts/packages/get_deps.py $packdir/pack.json 'pip')

echo apt: $aptdeps
echo pip: $pipdeps

# run installation scripts
if [ -f install/install.sh ]; then
    bash install/install.sh
fi

# if there exists a file called startup.sh in the install folder, create a systemd service based on the parameters in pack.json
if [ -f install/startup.sh ]; then
    servicefile="$(echo $packns | sed 's/\./-/g').service"
    packdesc=$(cat pack.json | grep description | sed -E 's/.*\"(.*)\",/\1/g')
    packname=$(cat pack.json | grep "\"name\"" -m 1 | sed -E 's/.*\"(.*)\",/\1/g')
    cat /home/pi/razhome/razhome-scripts/packages/template.service |
        sed 's@DESC@'"$packdesc"'@g' | sed 's@NAME@'"$packname"'@g' |
        sed 's@NS@'"$packns"'@g' >$packdir/$servicefile
    installService
fi

# add info to the database
curl -X POST -H "Content-Type: application/json" -d @pack.json localhost:3001/api/packs
