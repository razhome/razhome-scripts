#!/usr/bin/python3
from update_checker import UpdateChecker
import signal
from time import sleep

def main():
    checker = UpdateChecker()
    checker.startChecker()
    try:
        while(checker.thread.is_alive()):
            checker.thread.join(1)
    except KeyboardInterrupt:
        checker.stopChecker()
        if(checker.installer.running):
            checker.installer.thread.join()
    except SystemExit:
        checker.stopChecker()
        if(checker.installer.running):
            checker.installer.thread.join()


if(__name__ == "__main__"):
    sleep(10)
    main()