#!/usr/bin/python3

from subprocess import run
from sys import platform
import threading
from time import sleep
from pathlib import Path

class Installer:
    """Simple class for running installer operations one at a time in a separate thread.
    The idea is that if one operation is taking a long time, we can still be queuing installs
    and not worry about modifying resources at the same time or overloading the Pi.
    """

    def __init__(self):
        self.queue = []
        self.thread = threading.Thread()
        self.running = False

    def queueInstall(self, path):
        """ Adds the given file to the install queue and starts the Installer thread
            if it is not already running
        
            Arguments:
                path {`str`} -- The path to the downloaded file which is to be installed
        """
        self.queue.append(path)

        if not self.running:
            self.startThread()

    def doInstall(self, path):
        result = run(['bash', '/home/pi/razhome/razhome-scripts/packages/install-pack.sh', path])
        if(result.returncode != 0):
            print("WARNING: %s failed to install with error code %d" % path, result.returncode)
            return False
        else:
            print("Successfully installed", path)
            return True

    def startThread(self):
        self.thread = threading.Thread(target=self.worker, name="Installer")
        self.thread.start()
        self.running = True

    def worker(self):
        print("Installer thread starting")
        while(len(self.queue) > 0):
            print('Installing', self.queue[0])
            if(platform == 'win32'): # Windows debug purposes. Remove later.
                sleep(0.5)
                Path(self.queue[0]).unlink()
            else:
                if(self.doInstall(self.queue[0])):
                    Path(self.queue[0]).unlink()

            self.queue.pop(0)
            
        self.running = False
        print("Installer thread stopping")