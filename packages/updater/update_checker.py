#!/usr/bin/python3
"""
A class to be used for update checking and asyncronous installing
"""

import requests
import pathlib
import re
import threading
from installer import Installer
from time import sleep
import uuid


class UpdateChecker:
    """ Class for continuously checking for updates. Currently, the class checks for updates
        every 5 minutes, and checks for an interrupt, (which comes from `self.stopChecker()`
        or `self.forceUpdate()`) This is run asyncronously, and the `Thread` object is exposed
        through `self.thread`.
    """

    def __init__(self):
        self.installer = Installer()
        self.thread = threading.Thread()
        self.running = False
        self.interrupted = True

    def _dequeuePackage(self):
        """ Dequeues a package from the database and returns the name
        
            Returns:
                `str` | `bool` -- Returns the name of the blob if an item was dequeued, otherwise, returns False`
        """
        res = requests.post('http://localhost:3001/api/azure/blobs/dequeue')
        if(res.status_code == 200):
            return res.json()['blob']['name']
        else:
            return False


    def _downloadPackage(self, blobName):
        """ Downloads a package from the specified blob and saves it in the tmp directory

            Arguments:
                blobName {str} -- The name of the blob to be downloaded. Retreived from dequeuing a blob
            Returns:
                str|bool -- If the file downloaded successfully, returns the path of the file. Otherwise, returns true
        """

        tmpDir = pathlib.Path("./tmp")
        # if the tmp directory doesn't exist, makes one
        tmpDir.mkdir(exist_ok=True)

        res = requests.get(
            'http://localhost:3001/api/azure/download?name=' + blobName)
        if(res.status_code == 200):
            filename = re.findall(r".*\"(.*)\"", res.headers['Content-Disposition'])[0] + str(uuid.uuid4())
            with (tmpDir / filename).open('wb') as fd:
                for chunk in res.iter_content(chunk_size=1024):
                    fd.write(chunk)
            return "./tmp/" + filename
        else:
            print("Error Downloading File")
            print("Status Code:", res.status_code)
            print("Data:", res.json(), sep="\n")
            return False


    def _checkUpdates(self):
        """ Checks for updates once and queues updates into the installer if any are found.
            Only runs if `self.running == True`.
        """
        if(self.running):
            print("Checking for updates")
            blobs = []
            res = requests.post("http://localhost:3001/api/azure/blobs/dequeue")
            while(res.status_code == 200):
                blobs.append(res.json()['blob']['name'])
                res = requests.post("http://localhost:3001/api/azure/blobs/dequeue")

            for blob in blobs:
                dl_res = self._downloadPackage(blob)
                if(dl_res):
                    print("Queuing", dl_res, "for install.")
                    self.installer.queueInstall(dl_res)

    def startChecker(self):
        """ Starts the UpdateChecker thread.
        """
        if not self.running:
            self.thread = threading.Thread(target=self.worker, name="UpdateChecker")
            self.running = True
            self.thread.start()
        else:
            print("Warning: attempted start UpdateChecker thread while it was already running")

    def stopChecker(self):
        """ Stops the checker at the next interval by setting `interrupted` to `True` and `running` to `False`
        """
        self.running = False
        self.interrupted = True

    def forceCheck(self):
        self.interrupted = True

    def worker(self):
        """ While running, checks for updates every 300 seconds or 5 minutes.
            The timer is a series of 0.25 second delays, which allows more flexibility.
            Update checks can be forced by calling `forceCheck()`, which will cause
            a check with in 0.25 seconds.
        """

        print("Starting UpdateChecker thread")
        self._checkUpdates()
        while(self.running):
            c = 60
            while(c > 0):
                if(self.interrupted):
                    break
                sleep(0.25)
                c -= 1
            self.interrupted = False
            self._checkUpdates()

        print("UpdateChecker thread stopping")