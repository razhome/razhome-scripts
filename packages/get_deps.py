#!/usr/bin/python
import json, pathlib, sys

pack=json.loads(pathlib.Path(sys.argv[1]).read_text())

if('deps' in pack and sys.argv[2] in pack['deps']):
    print(' '.join(pack['deps'][sys.argv[2]]))