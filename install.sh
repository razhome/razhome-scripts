#!/bin/bash
set -e

echoerr() { >&2 echo "$@"; }

if [ $UID != 0 ]; then
    echoerr "ERROR: This script must be run as root"
    exit 1
fi

if [ -f "iam-key.json"]; then
    echo "Located google cloud credentials"
else
    echoerr "ERROR: Cannot find google cloud credentials (iam-key.json)."
    echoerr "Go to https://cloud.google.com/text-to-speech/docs/quickstart-client-libraries and follow"
    echoerr "steps 1-4 to aquire one. Then place it in /home/pi directory and run the script again"
    exit 1
fi

if [ -f "mongo.json" ]; then
    echo "Located MongoDB credentials"
else
    echoerr "ERROR: Cannot find MongoDB credentials"
    echoerr "If you haven't already, create a json file with the following keys:"
    echoerr "username : The username for an account with all CRUD permissions"
    echoerr "password : Password to that user."
    echoerr "uri : the rest of the connection string (should start with @"
    exit 1
fi

initdir=$(pwd)

# update and install necessary packages
apt update
apt upgrade
# add node.js repo
curl -sL https://deb.nodesource.com/setup_12.x | bash -
apt install -y python3-dev python3-pip git python3-all-dev \
               portaudio19-dev python3-pyaudio sox swig libpcre3 \
               libpcre3-dev libatlas-base-dev nodejs sox \
               libatlas-base-dev libasound-dev flac

# install necessary python packages
echo "installing necessary pip packages"
pip3 install SpeechRecognition
pip3 install google-cloud-texttospeech
pip3 install google-cloud-speech
pip3 install google-api-python-client
pip3 install oauth2client
pip3 install requests

pushd /home/pi

# build and install snowboy
echo "building snowboy python bindings"
mkdir libs
pushd libs
git clone https://github.com/Kitt-AI/snowboy.git
pushd snowboy
pushd swig/Python3
make
popd
pip3 install .
popd
popd

# clone all other repos
echo "cloning razhome repos"
mkdir razhome
pushd razhome
git clone https://gitlab.com/razhome/razhome-scripts.git
git clone https://gitlab.com/razhome/razhome-speech-daemon.git
git clone https://gitlab.com/razhome/razhome-db-api.git

# setup razhome-db-api
echo "seting up razhome-db-api"
pushd razhome-db-api
npm install --save
npm install --global nodemon
echo "adding mongo credentials"
mkdir creds
mv "$initdir/mongo.json" "creds/mongo.json"
popd

# setup razhome-scripts
echo "setting up razhome-scripts"
pushd razhome-scripts/runtime
bash "services.sh -y"
popd
chmod +x razhome-scripts/install-pack.sh

# setup razhome-speech-daemon
echo "setting up razhome-speech-daemon"
mkdir creds
mv "$initdir/iam-key.json" "creds/iam-key.json"
echo "$initdir/iam-key.json" | tee -a /etc/environment

# setup package directory
echo "setting up inital package directory"
mkdir packages
popd

echo "Completed install of razhome. Restart to apply changes"